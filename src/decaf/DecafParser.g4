parser grammar DecafParser;

@header {
package decaf;
}

options
{
  language=Java;
  tokenVocab=DecafLexer;
}

program: TK_class  LCURLY field_decl* method_decl* RCURLY;

field_decl: par (VIRGULA par)* PONTOVIR | par LRICOCHETE int_literal RRICOCHETE  (VIRGULA par LRICOCHETE int_literal RRICOCHETE)*  PONTOVIR;


method_decl: (type|VOID) ID LPARENT(par(VIRGULA par)*)? RPARENT block;

par: type? ID;

block: LCURLY (var|statement)* RCURLY;

var: par (variaveis)*PONTOVIR;
variaveis: VIRGULA par;

type: INT|BOOLEAN | VOID;

statement: location assign_op expr PONTOVIR
                    |method_call PONTOVIR
                    |SE LPARENT expr RPARENT block (ELSE block)?
                    |PARA ID assign_op expr VIRGULA expr block
                    |RETURN (expr)? PONTOVIR
                    |BREAK PONTOVIR
                    |CONTINUAR PONTOVIR
                    |block;

assign_op: ATRIBUICAO
          |DECREMENTO
          |INCREMENTO;

method_call: method_name LPARENT (expr (VIRGULA expr)*)? RPARENT
           | CALL LPARENT(string_literal (VIRGULA call_arg(VIRGULA call_arg)*)?) RPARENT;

method_name:ID;

location:ID
         |ID LRICOCHETE expr RRICOCHETE;

expr: location| method_call
     |literal
     |expr bin_op expr
     |SUBTRAIR expr 
     |EXCLA expr
     |LPARENT expr RPARENT;

call_arg: expr | string_literal;

bin_op: ar | rel | eq | cond;

ar:MULTIP|DIVIDIR|SUBTRAIR|SOMA|PORCENT|CHAPEL;
rel:MAIOR|MENOR|MAIORIG|MENORIG;
eq:IGUALDADE|DIFERETE;
cond:AND|OR;

literal: int_literal|char_literal|bool_literal;

int_literal: dec|hexaliteral;
dec: NT;
hexaliteral: HEXALITERAL;
bool_literal: BOOL;
char_literal: CHARLITERAL;
string_literal: STRINGLITERAL;
