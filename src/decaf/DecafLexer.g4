lexer grammar DecafLexer;

@header {
package decaf;
}

options
{
  language=Java;
}

TK_class: 'class Program';

LCURLY : '{'; // 1
RCURLY : '}'; //1
LPARENT: '(';//1
RPARENT: ')'; //1
LRICOCHETE: '[';//1
RRICOCHETE: ']'; //1
PONTOVIR: ';'; //1
VIRGULA: ',';//1
MULTIP: '*'; //1
PERGUNTA : '?'; //1 VER
DIVIDIR: '/'; //1
SUBTRAIR: '-'; // 1
PORCENT: '%'; //1
SOMA: '+';//1
CHAPEL: '^'; //1
EXCLA: '!';//1
AND: '&&'; //1
OR: '||'; //1

IGUALDADE: '=='; //1
DIFERETE: '!=';//1
MAIOR: '>'; //1
MENOR: '<'; //1
MAIORIG:'>='; // 1
MENORIG: '<='; // 1
INCREMENTO: '+='; // 1
DECREMENTO:'-='; // 1
ATRIBUICAO: '='; // 1
fragment PROGRAMA: 'Program';
fragment CLASS : 'class';
BOOLEAN: 'boolean'; //1
CALL : 'callout'; //1
ELSE : 'else';//1
SE : 'if'; //1
fragment FALSO : 'false';//1
INT : 'int';//1
RETURN : 'return'; //1
fragment VERDADE : 'true';//1
VOID: 'void';//1
PARA : 'for'; //1
BREAK: 'break'; //1
CONTINUAR: 'continue'; //1

 
NT: [0-9]+;

HEXALITERAL: '0x' (NT|'a'..'f'|'A'..'F' )+;


BOOL: VERDADE | FALSO;

ID: ('_'|C)('_'|C|NT)*; 


WS_ : (' ' | '\n' | '\t') -> skip;

SL_COMMENT : '//' (~'\n')* '\n' -> skip;

fragment CHAR : (ES|C|NT|RCURLY|LPARENT|RPARENT|LRICOCHETE|
PONTOVIR|
VIRGULA|
MULTIP|
DIVIDIR|
SUBTRAIR|
PORCENT|
SOMA|
CHAPEL|
AND|
OR|
IGUALDADE|
DIFERETE|
MAIOR|
MENOR|
MAIORIG|
MENORIG|
INCREMENTO|
DECREMENTO|
ATRIBUICAO|
PROGRAMA|
CLASS|
BOOLEAN|
CALL|
ELSE|
SE|
FALSO|
RETURN|
VERDADE|
VOID|
PARA|
BREAK|
CONTINUAR|
HEXALITERAL|
LCURLY);

CHARLITERAL : '\'' (ESC|CHAR) '\''; 

STRINGLITERAL : '"' (ESC|CHAR|~'"')* '"';


fragment C : ('a'..'z'|'A'..'Z');

fragment ESC :  '\\' ('"'|'n'|'t'|'\\'|'\''); //'r'

fragment ES: '#'|'$'|'&'|':'|'@'|'_'|'~'|' ';
